function parseJwt(jwt) {
	var payload = jwt.split('.')[1];
    var base64 = payload.replace('-', '+').replace('_', '/');
    // return payload;
    return JSON.parse(window.atob(base64)).id;
}


$(document).ready(function() {
  var myAccessToken;
  var nowTime = $.now();
  var nowTime2 = new Date();
  var timestamp = Math.floor($.now() / 1000);
  var myServiceId;

  var domain = "https://api.hanwhasecurity-vsaas.com";

  $("#number-creation").val(timestamp);



  $("#btn-get-accesstoken").click(function(){
    var apiUrl = domain +"/oauth2/token";

    var myAuth = "Basic " + $("#txt-auth").val();
    var myGrantType = $("#txt-grant").val();
    myWid = $("#txt-wid").val();
    myPwd = $("#txt-pwd").val();
    var myRedirect = $("#txt-redirect").val();

    var reqAccessToken = apiUrl + "?grant_type="+myGrantType + "&username=" + myWid + "&password=" + myPwd + "&redirect_uri=" + myRedirect;

    $.ajax({type:'POST', url: reqAccessToken, dataType: "json",
      headers: {
        "Content-Type":"application/json",
        "Authorization": myAuth
      },

      success: function(jsonData, textStatus, jqXHR) {
        myAccessToken = jsonData.access_token;

        console.log(myAccessToken);

       $("#txt-accesstoken1").val(myAccessToken);
      },
      error: function(jsonData, textStatus, jqXHR) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }      
    });
  });

 
  $("#select-period").change(function() {
    var creationDate = parseInt($("#number-creation").val());
    var servicePeriod = $("#select-period option:selected").val();
    var expireDate = $("#number-expire").val(creationDate + servicePeriod * 24 * 60 * 60);
  });

  
  $("#btn-reg-service").click(function() {
    var apiUrl = domain + "/internal/services";

    var camWid = $("#txt-camwid1").val();
    var serviceCode = $("#txt-serviceCode").val();
    var billingType = $("#txt-billingType").val();

    var creationDate = parseInt($("#number-creation").val());
    var expireDate = parseInt($("#number-expire").val());

    $.ajax({type: 'POST', url: apiUrl, dataType:"json",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer "+ myAccessToken
      },
      
      data: JSON.stringify({
        "data": {
          "cameraWid": camWid,
          "serviceCode": serviceCode,
          "billingType": billingType,
          "creationDate": creationDate,
          "updateDate": creationDate,
          "expirationDate": expireDate
        }
      }),

      success: function(jsonData, textStatus, jqXHR) {
        alert(textStatus + " to register the Service !");

      },

      error: function(jsonData, textStatus, err) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
  });
  

  $("#btn-search-service").click(function() {
    var camWid = $("#txt-camwid2").val();
    var apiUrl = domain + "/internal/cameras/"+camWid + "/services";
    var tag = '<div class="form-group row"> </div>';
    $("#textarea-group-service").html(tag);      

    $("#txt-endpoint3").val(apiUrl);

    $.ajax({type: 'GET', url: apiUrl, dataType: "json",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + myAccessToken
      },

      success: function(jsonData, textStatus, jqXHR) {
        var numberOfService = Object.keys(jsonData.data).length;
        var services = [];

        for(var i = 0; i<=numberOfService-1;i++) {
          (function(m) {
            var tag = '<div class="form-group row"> <textarea class="form-control" rows="10" id="textarea-service'+m+'" disabled=""></textarea> <button type = "button" class="btn btn-outline-primary btn-sm" data-toggle="button" aria-pressed="false" autocomplete="off" id = "btn-select-service'+m+'">Select</button></div>';
            $("#textarea-group-service").append(tag);  
            services[m] = JSON.stringify(jsonData.data[m], undefined, 10); 

            $("#textarea-service"+m).text(services[m]);
            
            $("#btn-select-service"+m).on('click',function() {
                myServiceId = jsonData.data[m].id;
                $("#txt-endpoint4").val(domain+ "/internal/services/"+myServiceId);

            });           
          })(i);
        }
        console.log(textStatus + " to get services");
      },

      error: function(jsonData, textStatus, jqXHR) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
  });


  $("#btn-detach-service").click(function(){
    var apiUrl = $("#txt-endpoint4").val();
    var camWid = $("#txt-camwid3").val();

    $.ajax({type: 'PUT', url: apiUrl, dataType: "json",
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + myAccessToken
      },

      data: JSON.stringify({
        "data": {
            "cameraWid": camWid
        }        
      }),

      success: function(jsonData, textStatus, jqXHR) {
        alert(textStatus + " to detach service from camera");

      },

      error: function(jsonData, textStatus, jqXHR) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);        
      }

    });

  });













































  $("#btn-get-timestamp").click(function(){
    if($("#txt-accesstoken1").val()){


    var timestamp = Math.floor($.now() / 1000);
    $("#txt-timestamp").val(timestamp);
    }
    else{
      alert("Get Access Token !");
    }
  });


  $("#btn-set-metadata").click(function() {


    var eventType = $("#select-eventType option:selected").val();
    myFileTimeStamp = parseInt($("#txt-timestamp").val());
    var duration = parseInt($("#txt-duration").val());
    var chunkPosition = $("#select-chunkPosition option:selected").val();


    if($("#txt-timestamp").val()) {

      myMetadata.data.duration = duration;
      myMetadata.data.fileTimestamp = myFileTimeStamp;
      myMetadata.data.chunk.eventType = eventType;
      myMetadata.data.chunk.chunkPosition = chunkPosition;

      $("#textarea-meta").val(JSON.stringify(myMetadata, undefined, 10)); 
    }

    else{
      alert("Get File Timestamp !");

    }

  });


  $("#btn-get-storageToken").click(function() {
    if(myFileTimeStamp) {
      var apiUrl = domain +"/identity/htwstorage1/token";
      $.ajax({type:'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },
        data: JSON.stringify({
          "cameraId": myCameraId,
          "type": myType,
          "fileTimestamp": myFileTimeStamp,
        }),

        success: function(jsonData, textStatus, jqXHR) {
          mySignedUrl =jsonData.data.storageLocation;
          $("#txt-signedUrl").val(mySignedUrl);
          console.log("signed url : \n"+mySignedUrl);
        }
      });      
    }
    else{
      alert("Set the metadata !");
    }
  });  

  $("#btn-put-file").click(function(){
    var file = $("#file-select-file").files;

      if($("#txt-signedUrl").val() && $("#file-select-file").val()){

        $.ajax({type:'PUT', url: mySignedUrl,
          headers: {"Content-Type": "video/mp4;charset=UTF-8"},
          data: file,

          success: function(jsonData, textStatus, jqXHR) {
            console.log(textStatus +" to upload mp4 !");
          }
        });      
      }
      else{
        alert("Get Storage Token or Select a file !!");
      }
  });


  $("#btn-post-meta").click(function() {
    if($("#textarea-meta").val()) {
      var apiUrl = domain + "/cameras/" + myCameraId + "/evideos";

      $.ajax({type:'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },
        data: JSON.stringify(myMetadata),

        success: function(jsonData, textStatus, jqXHR) {
          console.log(textStatus + " to upload Metadata !");
        }

        });
      }
    else{
      alert("Set the metadata !");
    }
  });
});


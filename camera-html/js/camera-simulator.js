function parseJwt(jwt) {
	var payload = jwt.split('.')[1];
    var base64 = payload.replace('-', '+').replace('_', '/');
    // return payload;
    return JSON.parse(window.atob(base64)).id;
}


$(document).ready(function() {
	var myWid;
	var myPwd;
	var myUserId;
	var myAccessToken;
	var myCameraId;
	var myType="camera";
  var myFileTimeStamp;
  var mySignedUrl;
  var myMetadata=
  {
    "data": {
        "type": "evideo",
      "fileTimestamp": 148597756,
        "duration": 60,
        "resolution": "640x480",
        "fps": 15,
        "quality": "AHD",
        "fileSize": 2560000,
        "fileType": "mp4",
        "fileName": "$YYYYMMDDHHMMSS.mp4",
        "fileContainer": "MP4",
        "videoCodec": "H.265",
        "audioCodec": "AAC",
        "chunk": {
          "eventType":"BODYDET",
          "chunkPosition":"SINGLE"
        },
        "snapshot": {
          "snapshotName":"$YYYYMMDDHHMMSS.jpg"
        }
    }
};
  var domain = "https://api.hanwhasecurity-vsaas.com";


	$("#btn-get-accesstoken").click(function(){
    var apiUrl = domain +"/oauth2/token";

      var myAuth = "Basic " + $("#txt-auth").val();
      var myGrantType = $("#txt-grant").val();
      myWid = $("#txt-wid").val();
      myPwd = $("#txt-pwd").val();
      var myRedirect = $("#txt-redirect").val();

      var reqAccessToken = apiUrl + "?grant_type="+myGrantType + "&username=" + myWid + "&password=" + myPwd + "&redirect_uri=" + myRedirect;

    $.ajax({type:'POST', url: reqAccessToken, dataType: "json",
      headers: {
        "Content-Type":"application/json",
        "Authorization": myAuth
      },

      success: function(jsonData, textStatus, jqXHR) {
        myCameraId = jsonData.id;
        myAccessToken = jsonData.access_token;

        console.log(myAccessToken);

       $("#txt-userid").val(myCameraId);
       console.log(myCameraId);

       $("#txt-accesstoken1").val(myAccessToken);
       $("#txt-accesstoken2").val(myAccessToken);
      },
      error: function(jsonData, textStatus, jqXHR) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
  });

  $("#btn-get-timestamp").click(function(){
    if($("#txt-accesstoken1").val()){


    var timestamp = Math.floor($.now() / 1000);
    $("#txt-timestamp").val(timestamp);
    }
    else{
      alert("Get Access Token !");
    }
  });


  $("#btn-set-metadata").click(function() {


    var eventType = $("#select-eventType option:selected").val();
    myFileTimeStamp = parseInt($("#txt-timestamp").val());
    var duration = parseInt($("#txt-duration").val());
    var chunkPosition = $("#select-chunkPosition option:selected").val();


    if($("#txt-timestamp").val()) {

      myMetadata.data.duration = duration;
      myMetadata.data.fileTimestamp = myFileTimeStamp;
      myMetadata.data.chunk.eventType = eventType;
      myMetadata.data.chunk.chunkPosition = chunkPosition;

      $("#textarea-meta").val(JSON.stringify(myMetadata, undefined, 10)); 
    }

    else{
      alert("Get File Timestamp !");
    }

  });


  $("#btn-get-storageToken").click(function() {
    if(myFileTimeStamp) {
      var apiUrl = domain +"/storage1/token";
      $.ajax({type:'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },
        data: JSON.stringify({
          "cameraId": myCameraId,
          "type": myType,
          "fileTimestamp": myFileTimeStamp,
        }),

        success: function(jsonData, textStatus, jqXHR) {
          mySignedUrl =jsonData.data.storageLocation;
          $("#txt-signedUrl").val(mySignedUrl);
          console.log("signed url : \n"+mySignedUrl);
        }
      });      
    }
    else{
      alert("Set the metadata !");
    }
  });  

  $("#btn-put-file").click(function(){
    var files = document.getElementById("file-select-file").files;
    var file = files[0];


    // var file = $("#file-select-file").files[0];
    // var formData = file.

      if($("#txt-signedUrl").val() && $("#file-select-file").val()){

        $.ajax({type:'PUT', url: mySignedUrl,
          headers: {"Content-Type": "video/mp4;charset=UTF-8"},
          data: file,
          processData: false,

          success: function(jsonData, textStatus, jqXHR) {
            console.log(textStatus +" to upload mp4 !");
          }
        });      
      }
      else{
        alert("Get Storage Token or Select a file !!");
      }
  });


  $("#btn-post-meta").click(function() {
    if($("#textarea-meta").val()) {
      var apiUrl = domain + "/cameras/" + myCameraId + "/evideos";

      $.ajax({type:'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },
        data: JSON.stringify(myMetadata),

        success: function(jsonData, textStatus, jqXHR) {
          console.log(textStatus + " to upload Metadata !");
        }

        });
      }
    else{
      alert("Set the metadata !");
    }
  });
});


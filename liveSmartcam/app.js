
var express = require('express');
var path = require('path');
var http = require('http');
var https = require('https'),
    express = require('express'),
    fs = require('fs'),
    options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};


var port = 8000;
var app = express();
app.use(express.static(path.join(__dirname, 'public')));

// var server = https.createServer(options, app).listen(port, function(){
//   console.log("Https server listening on port " + port);
// });

var server = http.createServer(app).listen(port, function(){
  console.log("Https server listening on port for SMARTCAM through " + port);
});


app.get('/smartcam', function (req, res) {
    // res.writeHead(200, {'Content-Type' : 'text/html'});
    // res.write('<h3>Welcome</h3>');
    // res.write('<a href="/login">Please login</a>');
    // res.end();
    fs.readFile('index_cli.html', function(error, data){
    // fs.readFile('jingle_interop_testing2.html', function(error, data){    
        res.writeHead(200, {'Content-Type' : 'text/html'});
        res.end(data);
        }
    )

});

app.get('/dist/index.js', function (req, res) {
    fs.readFile('./public/dist/index.js', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/javascript'});
        res.end(data);
    });
});

app.get('/dist/jquery-ui.css', function(req, res) {
    fs.readFile('./public/dist/jquery-ui.css', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/css'});
        res.end(data);
    });
});


app.get('/dist/index.css', function(req, res) {
    fs.readFile('./public/dist/index.css', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/css'});
        res.end(data);
    });
});


app.get('/dist/jquery-ui.js', function(req, res) {
    fs.readFile('./public/dist/jquery-ui.js', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/css'});
        res.end(data);
    });
});


var https = require('https');
var express = require('express');
var fs = require('fs');

var app = express();
var port = 2000;
var options = {
	key: fs.readFileSync('./key.pem'),
	cert: fs.readFileSync('./cert.pem')
}

var AWS = require('aws-sdk');

https.createServer(options, app).listen(port, function(){


	console.log("Auth Server listening on port " + port);

	makeKey();
});



// infromation about Cloud Path, Local Path, File, etc. 
//--------------------------------------------------------------------------//

	var serviceName = 's3';
	var bucketName = 'htw-evideos-v1';
	var cameraName = '12345678-9012-3456-7890-123456789012';
	var regionName = 'us-east-1';

	var partial_cameraname = cameraName.split("-");

	var DDMMYYYY='09022017';
	var cameraModel='doorbell-001';
	var cameraUuid= partial_cameraname[0] + partial_cameraname[1] + partial_cameraname[2] + partial_cameraname[3] + partial_cameraname[4];
	var accountNo = '388591930080%3A';


	var cloudPath = '/' + DDMMYYYY +'/'+ cameraModel +'/'+ accountNo + cameraUuid + '/';
	var localPath = './'

	var fileName = '201701020304-123456789012345678901234567890123456.mp4';

//--------------------------------------------------------------------------//

	var temp_auth, temp_accessKey, temp_secretKey, temp_sessionToken;

        var date = new Date();
        var datetime = AWS.util.date.iso8601(date).replace(/[:\-]|\.\d{3}/g, '');



function makeKey() {
	var IAMaccessKeyId;
	var IAMsecretAccessKey;

	var type;
	var typeNum = 3; //변수로 입력 받아서



	var durationHour;
	var durationSecond;

	var IAMaccessKeyId, IAMsecretAccessKey;



    switch(typeNum) {
    	
    	case 1:
    	//camera(only putObject)
    		type = 'camera';
	        durationHour = 36; // min = 0.25, max = 36
	        durationSecond = durationHour * 60 * 60;
	 
	        IAMaccessKeyId = 'AKIAIFTL6YC5LHPCYQVQ';
	        IAMsecretAccessKey = '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';    	

    		break;
    	
	    case 2:
	    //mobile/web(only getObject)
	    	type = 'mobile/web';
	        durationHour = 1;
	        durationSecond = durationHour * 60 * 60;

	        IAMaccessKeyId = 'AKIAJTCP5KF7LYATGJAQ';
	        IAMsecretAccessKey = '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';
    		
    		break;
    	
    	case 3:
    	//api server (all action is allowed)
	        type = 'root';        
	        durationHour = 36;
	        durationSecond = durationHour * 60 * 60;

	        IAMaccessKeyId = 'AKIAJ4WH7YH55BFCLW7A';
	        IAMsecretAccessKey = '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';
	        
	        break;


	    default :
	    	console.log("[Error] invalid type number");

    }

    //sign with IAM Credentials
    AWS.config.update({
        accessKeyId: IAMaccessKeyId,
        secretAccessKey: IAMsecretAccessKey,
        region: regionName
    });


    var sts = new AWS.STS({apiVersion: '2011-06-15'});
    var param = {Name: cameraUuid, DurationSeconds: durationSecond};


    sts.getFederationToken(param, function(err, data) {
    	if(err) {
    		console.log(err);
    	}

    	else {

    		temp_accessKey = data.Credentials.AccessKeyId;
    		temp_secretKey = data.Credentials.SecretAccessKey;
    		temp_sessionToken = data.Credentials.SessionToken;

	
    		//sign with Temporary Credentials
    		AWS.config.clear();
    		AWS.config.update({
    			region: regionName,
    			accessKeyId: temp_accessKey,
    			secretAccessKey: temp_secretKey,
    			sessionToken: temp_sessionToken
    		});

    		console.log("\n--------------------------Temporary Credentials -------------------------");
    		console.log("Type : " + type);
            console.log("aws_temp_ak : " + AWS.config.credentials.accessKeyId);
            console.log("aws_temp_sk : " + AWS.config.credentials.secretAccessKey);
            console.log("aws_temp_tk : " + AWS.config.credentials.sessionToken);

	
		console.log("\nS3 directory Path : https://s3.amazonaws.com/" + bucketName +  cloudPath + fileName);
            console.log("-------------------------------------------------------------------------");

	makePutRequest();
	makeGetRequest();
	

    	}
    });



function makePutRequest() {

	  var putRequest = {
	  	method: 'PUT',
	  	region:regionName,
	  	headers : {
			'Content-Type':'application/octet-stream',
//			'Host':bucketName+'.'+serviceName+'.'+regionName+'.amazonaws.com',  //Seoul 용 host (sigv4)
//			'Host':bucketName + '.' + serviceName + '-' + regionName + '.amazonaws.com', //N. California 용 host (sigv2)
//			'Host':'s3.amazonaws.com',  //us-east-1 && bucetName 에 '.' 포함
			'Host':bucketName + '.' + serviceName + '.amazonaws.com', // standard 용 host (sigv2)
	  		'X-Amz-Content-Sha256':'UNSIGNED-PAYLOAD',
	  		'X-Amz-User-Agent':'aws-sdk-js/2.6.10'
	  	},
	  	search : function() {
	  		return '';
	  	},
	  	pathname : function() {
	  		return cloudPath+fileName;
	  	}
	  };

	

	var signer = new AWS.Signers.V4(putRequest, 's3', false);


		signer.addAuthorization(AWS.config.credentials, date);
		signer.addHeaders(AWS.config.credentials, datetime);
		

		var signatureValue = signer.signature(AWS.config.credentials, datetime);


		/*Print Details for Sigv4*/

                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\nsignature : \n"+signer.signature(AWS.config.credentials, datetime));  
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\nstring To sign : \n"+signer.stringToSign(datetime)+"\n");
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\ncanonicalString : \n"+signer.canonicalString()+"\n");         
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\ncanonicalHeaders : "+signer.canonicalHeaders()+"\n");         
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\nsignedHeaders : \n"+signer.signedHeaders()+"\n");     
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\ncredentialString : \n"+signer.credentialString(datetime)+"\n");  
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\nauthorization : \n"+signer.authorization(AWS.config.credentials, datetime)+"\n");                     
                // console.log("///////////////////////////////////////////////////////////////");
                // console.log("\nhexEncodedBodyHash : \n"+signer.hexEncodedBodyHash()+"\n"); 


	console.log("\n------------------------------- putObject -------------------------------");
        console.log("curl 'https://"+bucketName+"."+serviceName+".amazonaws.com"+ cloudPath +fileName+"' -v -X PUT -T '"+localPath+fileName+"' -H 'Authorization: AWS4-HMAC-SHA256 Credential="+temp_accessKey+"/"+datetime.substr(0,8)+"/"+regionName+"/"+serviceName+"/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date;x-amz-security-token;x-amz-user-agent, Signature="+signatureValue+"' -H 'X-Amz-Content-Sha256: UNSIGNED-PAYLOAD' -H 'x-amz-security-token: "+temp_sessionToken+"' -H 'X-Amz-Date: "+datetime+"' -H 'X-Amz-User-Agent: aws-sdk-js/2.6.10'");

 //       console.log("curl 'https://s3.amazonaws.com/"+bucketName+  cloudPath +fileName+"' -v -X PUT -T '"+localPath+fileName+"' -H 'Authorization: AWS4-HMAC-SHA256 Credential="+temp_accessKey+"/"+datetime.substr(0,8)+"/"+regionName+"/"+serviceName+"/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date;x-amz-security-token;x-amz-user-agent, Signature="+signatureValue+"' -H 'X-Amz-Content-Sha256: UNSIGNED-PAYLOAD' -H 'x-amz-security-token: "+temp_sessionToken+"' -H 'X-Amz-Date: "+datetime+"' -H 'X-Amz-User-Agent: aws-sdk-js/2.6.10'");
}


function makeGetRequest() {

          var getRequest = {
                method: 'GET',
                region:regionName,
                headers : {
                        'Content-Type':'video/mp4',
//                      'Host':bucketName+'.'+serviceName+'.'+regionName+'.amazonaws.com',  //Seoul 용 host (sigv4)
//                      'Host':bucketName + '.' + serviceName + '-' + regionName + '.amazonaws.com', //N. California 용 host (sigv2)
//                      'Host':'s3.amazonaws.com',  //us-east-1 && bucetName 에 '.' 포함
                        'Host':bucketName + '.' + serviceName + '.amazonaws.com', // standard 용 host (sigv2)
                        'X-Amz-Content-Sha256':'UNSIGNED-PAYLOAD',
                        'X-Amz-User-Agent':'aws-sdk-js/2.6.10'
                },
                search : function() {
                        return '';
                },
                pathname : function() {
                        return cloudPath+fileName;
                }
          };



	var signer = new AWS.Signers.V4(getRequest, 's3', false);	

		signer.addAuthorization(AWS.config.credentials, date);
		signer.addHeaders(AWS.config.credentials, datetime);
		

		var signatureValue = signer.signature(AWS.config.credentials, datetime);


		/*Print Details for Sigv4*/

	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\nsignature : \n"+signer.signature(AWS.config.credentials, datetime));	
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\nstring To sign : \n"+signer.stringToSign(datetime)+"\n");
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\ncanonicalString : \n"+signer.canonicalString()+"\n");  	
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\ncanonicalHeaders : "+signer.canonicalHeaders()+"\n");  	
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\nsignedHeaders : \n"+signer.signedHeaders()+"\n");  	
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\ncredentialString : \n"+signer.credentialString(datetime)+"\n");  
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\nauthorization : \n"+signer.authorization(AWS.config.credentials, datetime)+"\n"); 	  		
	  	// console.log("///////////////////////////////////////////////////////////////");
	  	// console.log("\nhexEncodedBodyHash : \n"+signer.hexEncodedBodyHash()+"\n"); 


        console.log("\n------------------------------- getObject -------------------------------");
        console.log("curl -v -o '"+localPath+fileName+"' 'https://"+bucketName+".s3.amazonaws.com"+ cloudPath +fileName+"' -H 'Authorization: AWS4-HMAC-SHA256 Credential="+temp_accessKey+"/"+datetime.substr(0,8)+"/"+regionName+"/"+serviceName+"/aws4_request, SignedHeaders=host;x-amz-content-sha256;x-amz-date;x-amz-security-token;x-amz-user-agent, Signature="+signatureValue+"' -H 'X-Amz-Content-Sha256: UNSIGNED-PAYLOAD' -H 'x-amz-security-token: "+temp_sessionToken+"' -H 'X-Amz-Date: "+datetime+"' -H 'X-Amz-User-Agent: aws-sdk-js/2.6.10'");
	console.log("-------------------------------------------------------------------------");
  
	}
}

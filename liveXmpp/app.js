
var express = require('express');
var path = require('path');
var proxy = require('http-proxy-middleware');
var app = express();
var https = require('https'),
    express = require('express'),
    fs = require('fs'),
    options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

var port = 80;
https.createServer(options, app).listen(port, function(){
  console.log("Https server listening on port for XMPP through  " + port);
});


// proxy middleware options 
var options = {
        target: 'http://23.20.215.209:5280/', // target host 
        changeOrigin: true,               // needed for virtual hosted sites 
        // ws: true,                         // proxy websockets 
        // pathRewrite: {
        //     '^/old/api' : '/new/api',     // rewrite path 
        //     '^/remove/api' : '/api'       // remove path 
        // },
        // proxyTable: {
        //     // when request.headers.host == 'dev.localhost:3000', 
        //     // override target 'http://www.example.org' to 'http://localhost:8000' 
        //     'dev.localhost:3000' : 'http://localhost:8000'
        // }
    };
 
// create the proxy (without context) 
var exampleProxy = proxy(options);

// app.use(express.static(path.join(__dirname, 'public')))

// mount `exampleProxy` in web server 
app.use('/http-bind', exampleProxy);
app.get('/', function (req, res) {
    // res.writeHead(200, {'Content-Type' : 'text/html'});
    // res.write('<h3>Welcome</h3>');
    // res.write('<a href="/login">Please login</a>');
    // res.end();
    fs.readFile('index.html', function(error, data){
    // fs.readFile('jingle_interop_testing2.html', function(error, data){    
        res.writeHead(200, {'Content-Type' : 'text/html'});
        res.end(data);
        }
    )

});

app.get('/xmpp', function (req, res) {
    // res.writeHead(200, {'Content-Type' : 'text/html'});
    // res.write('<h3>Welcome</h3>');
    // res.write('<a href="/login">Please login</a>');
    // res.end();
    fs.readFile('index_cli.html', function(error, data){
    // fs.readFile('jingle_interop_testing2.html', function(error, data){    
        res.writeHead(200, {'Content-Type' : 'text/html'});
        res.end(data);
        }
    )

});

app.get('/js/stropheSDK.js', function (req, res) {
    // res.writeHead(200, {'Content-Type' : 'text/html'});
    // res.write('<h3>Welcome</h3>');
    // res.write('<a href="/login">Please login</a>');
    // res.end();
    fs.readFile('./public/js/stropheSDK.js', function(error, data){
    // fs.readFile('jingle_interop_testing2.html', function(error, data){    
        res.writeHead(200, {'Content-Type' : 'text/javascript'});
        res.end(data);
        }
    )

});

app.get('/js/strophe.js', function (req, res) {
    // res.writeHead(200, {'Content-Type' : 'text/html'});
    // res.write('<h3>Welcome</h3>');
    // res.write('<a href="/login">Please login</a>');
    // res.end();
    fs.readFile('./public/js/strophe.js', function(error, data){
    // fs.readFile('jingle_interop_testing2.html', function(error, data){    
        res.writeHead(200, {'Content-Type' : 'text/javascript'});
        res.end(data);
        }
    );

});


app.get('/dist/index.js', function (req, res) {
    fs.readFile('./public/dist/index.js', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/javascript'});
        res.end(data);
    });
});

app.get('/dist/jquery-ui.css', function(req, res) {
    fs.readFile('./public/dist/jquery-ui.css', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/css'});
        res.end(data);
    });
});


app.get('/dist/index.css', function(req, res) {
    fs.readFile('./public/dist/index.css', function(error, data) {
        res.writeHead(200, {'Content-Type' : 'text/css'});
        res.end(data);
    });
});



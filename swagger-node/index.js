var express = require('express'); // 웹서버 사용 .
var https = require('https');
var app = express();
var fs = require('fs'); // 파일 로드 사용.
var path = require('path');
// const restify = require('restify');

var options = {  
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

 

// const server = restify.createServer({
//         certificate : certData,
//         key         : keyData,
//         name        : pkg.name,
//         version     : pkg.version
// })


// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//   next();
// });


var CORS = require('cors')();
 
// 마찬가지로 app.use(router)전에 삽입한다
app.use(CORS);



// server.use(restify.CORS())
// server.use(restify.fullResponse());
// restify.CORS.ALLOW_HEADERS.push('authorization');


app.use('/', express.static(path.join(__dirname, 'node_modules/swagger-ui/dist')));
// app.use('/css', express.static(path.join(__dirname, 'node_modules/swagger-ui/dist/lib')));
// app.use('/lib', express.static(path.join(__dirname ,'node_modules/swagger-ui/dist/lib')));


https.createServer(options, app).listen(2000, function() {
	console.log("Https server listening on port "+ 2000)
});

// 라우팅 설정
app.get('/', function (req, res) { // 웹서버 기본주소로 접속 할 경우 실행 . ( 현재 설정은 localhost 에 3303 port 사용 : 127.0.0.1:3303 )
	fs.readFile('node_modules/swagger-ui/dist/index.html', function (error, data) { // index.html 파일 로드 .
		if (error) {
			console.log(error);
		} 
		else {
			res.writeHead(200, { 'Content-Type': 'text/html' }); // Head Type 설정 .
			res.end(data); // 로드 html response .
		} 
	});
});



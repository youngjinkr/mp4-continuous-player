var client;

var WebrtcSDK = function(loadingParams)
{
  this.params = loadingParams;
}


WebrtcSDK.prototype.login = function(){
  console.dir(this.params);
  client = new Paho.MQTT.Client(this.params.mqttServerIP, this.params.id);
  client.connect({userName: this.params.user_name, password: this.params.password, onSuccess: this.onConnect, onFailure: this.failConnect,useSSL: false, keepAliveInterval: 10});
}


WebrtcSDK.prototype.failConnect = function (e){
   console.log("connect failed");
   console.log(e);
}

WebrtcSDK.prototype.onConnect =  function (){
   console.log("onConnect");
   $("#join").attr("disabled", false);
   // console.dir(this)
   // subscribe("offer");
}


WebrtcSDK.prototype.join = function(peerid){
  console.log("join");

  if(peerid === 'camera'){
    this.params.type = 'camera'
    this.subscribe("offer")
    this.startVideo();

  }else if(peerid !== 'camera'){
    console.log("send message")
    this.params.type = 'user'
    this.makeOffer(); 
  }

}



WebrtcSDK.prototype.subscribe = function(waitType) {
  console.log("hjkwon called subscribe")
  console.dir(this)
  // set callback
  client.onMessageArrived = this.onMessageArrived.bind(this);
   // Subscribe
  var topic = this.buildTopic(waitType,this.params.user_name);
  client.subscribe(topic);
}


WebrtcSDK.prototype.buildTopic = function(signalingType, user_name) {
  var topic = user_name + '/signaling/' + signalingType;
  return topic;
}

  // callback for receiving message
WebrtcSDK.prototype.onMessageArrived = function(message) {
  console.log("onMessageArrived:" + message.destinationName + ' -- ' + message.payloadString);
  console.dir(this)

  if (message.destinationName === this.buildTopic('answer',this.params.user_name)) {
    console.log("hjkwon answer")
    this.onAnswerText(message.payloadString)
  }
  else if (message.destinationName === this.buildTopic('offer',this.params.user_name)) {
    console.log("hjkwon offer")
    this.onOfferText(message.payloadString)
  }
  else {
    console.warn('Bad SDP topic');
}
}



WebrtcSDK.prototype.onOfferText = function(text) {
  console.log("Received offer text...")
  console.log(text);
  console.dir(this);
  this.setOfferText(text);
  this.makeAnswer();
}

WebrtcSDK.prototype.onAnswerText= function(text){
  console.log("Received answer text...")
  console.log(text);
  console.dir(this);
  this.setAnswerText(text);
}  



WebrtcSDK.prototype.setOfferText= function(text) {
  if (this.peerConnection) {
    console.error('peerConnection alreay exist!');
  }
  this.peerConnection = this.prepareNewConnection();
  var offer = new RTCSessionDescription({
    type : 'offer',
    sdp : text,
  });
  this.peerConnection.setRemoteDescription(offer);
}


WebrtcSDK.prototype.prepareNewConnection = function() {
var pc_config = {"iceServers":[]};
  // var pc_config = {"iceServers" :[{"urls":["turn:173.194.72.127:19305?transport=udp","turn:[2404:6800:4008:C02::7F]:19305?transport=udp","turn:173.194.72.127:443?transport=tcp","turn:[2404:6800:4008:C02::7F]:443?transport=tcp"],"username":"CP+UkbwFEgYD6JFBxo8Yzc/s6OMT","credential":"PcA9lisVrYtYGwJhL8xUX8H6qlI="},{"urls":["stun:stun.l.google.com:19302"]}]}
  var peer = null;
  try {
    peer = new webkitRTCPeerConnection(pc_config);
  } catch (e) {
    console.log("Failed to create peerConnection, exception: " + e.message);
  }

  // send any ice candidates to the other peer
  peer.onicecandidate = function (evt) {
    console.log("onicecandidate")
    if (evt.candidate) {
    console.log("hjkwon candidate :" + evt.candidate);
    } else {
    console.log("ice event phase =" + evt.eventPhase);
    console.log(this)
    this.sendSDPTextMQTT(this.peerConnection.localDescription.type, this.peerConnection.localDescription.sdp);
    }
  }.bind(this);

  peer.oniceconnectionstatechange = function() {
    console.log('ice connection status=' + peer.iceConnectionState + ' gahter=' + peer.iceGatheringState);
    if ('completed' === peer.iceConnectionState) {
    console.log("candidate complete");
    }
  };

  peer.onsignalingstatechange = function() {
    console.log('signaling status=' + peer.signalingState);
  };

  if(this.params.type === "camera"){
    console.log('Adding local stream...');
    peer.addStream(this.params.localStream);
  }
  
  peer.addEventListener("addstream", onRemoteStreamAdded.bind(this), false);
  peer.addEventListener("removestream", onRemoteStreamRemoved, false)

  // when remote adds a stream, hand it on to the local video element
  function onRemoteStreamAdded(event) {
    console.log("Added remote stream");
    console.dir(event.stream)
    console.log(this);
    this.params.remoteVideo.attr('src', webkitURL.createObjectURL(event.stream));
    this.params.remoteVideo[0].play();
    this.params.remoteVideo[0].autoplay = true;
    spinner.stop();

  }

  // when remote removes a stream, remove it from the local video element
  function onRemoteStreamRemoved(event) {
    console.log("Remove remote stream");
    remoteVideo.src = "";
  }

  return peer;
}



WebrtcSDK.prototype.makeOffer = function() {
   
   this.subscribe("answer");
   this.peerConnection = this.prepareNewConnection();
   this.peerConnection.createOffer(function (sessionDescription) { // in case of success
    console.log("setLocalDescription")
    console.dir(this)
    this.peerConnection.setLocalDescription(sessionDescription);
    console.log("Sending: offer SDP");
    console.log(sessionDescription);
   }.bind(this) , function () { // in case of error
    console.log("Create Offer failed");
   }, this.params.mediaConstraints);

}

WebrtcSDK.prototype.makeAnswer = function(evt) {
   console.log('sending Answer. Creating remote session description...' );
   if (! this.peerConnection) {
    console.error('peerConnection NOT exist!');
    return;
   }

   this.peerConnection.createAnswer(function (sessionDescription) { // in case of success
    this.peerConnection.setLocalDescription(sessionDescription);
    console.log("Sending: Answer SDP");
    console.log(sessionDescription);
   }.bind(this), function () { // in case of error
    console.log("Create Answer failed");
   }, this.params.mediaConstraints);
}

WebrtcSDK.prototype.setAnswerText= function(text) {
   if (! this.peerConnection) {
    console.error('peerConnection NOT exist!');
    return;
   }
   var answer = new RTCSessionDescription({
    type : 'answer',
    sdp : text,
   });
   this.peerConnection.setRemoteDescription(answer);
}


WebrtcSDK.prototype.startVideo = function() {
 var mediaConstraints = {video: {width:{min:1280}, height:{min:720}, frameRate:{ideal : 10, min :15}}, audio: false}
 requestUserMedia(mediaConstraints).then(function(stream) {
  console.log("Got access to local media with mediaConstraints:\n" + "  '" + JSON.stringify(mediaConstraints) + "'");
  console.dir(this) 
  this.params.localVideo.attr('src', webkitURL.createObjectURL(stream));
  this.params.localVideo[0].play();
  this.params.localVideo[0].autoplay = true;
  this.params.localStream = stream;
  // this.onUserMediaSuccess_(stream);
 }.bind(this)).catch(function(error) {
  // this.onError_("Error getting user media: " + error.message);
  // this.onUserMediaError_(error);
 }.bind(this));
}


WebrtcSDK.prototype.sendSDPTextMQTT = function(type, text){
   var topic = this.buildTopic(type,this.params.user_name);
   message = new Paho.MQTT.Message(text);
   message.destinationName = topic;
   client.send(message);
}



function requestUserMedia(constraints) {
  return new Promise(function(resolve, reject) {
    var onSuccess = function(stream) {
      resolve(stream);
    };
    var onError = function(error) {
      reject(error);
    };
    try {
      navigator.webkitGetUserMedia(constraints, onSuccess, onError);
    } catch (e) {
      reject(e);
    }
  });
}




// // stop local video
// function stopVideo() {
//   localVideo.src = "";
//   localStream.stop();
// }

//   function unsubscribe(waitType) {
//    // UnSubscribe 
//    var topic = buildTopic(waitType);
//    client.unsubscribe(topic);
//   }

// function onSDPText() {
// var text = textToReceiveSDP.value;
// if (peerConnection) {
// onAnswerText(text);
// }
// else {
// onOfferText(text);
// }
// textToReceiveSDP.value ="";
// }	



//   function sendSDPText(text) {
//    console.log("---sending sdp text ---");
//    console.log(text);

//    textForSendSDP.value = text;
//    textForSendSDP.focus();
//    textForSendSDP.select();
//   }

//   function hangUp() {
//    console.log("Hang up.");
//    stop();
//   }

//   function stop() {
//    peerConnection.close();
//    peerConnection = null;
//    peerStarted = false;
//   }


  





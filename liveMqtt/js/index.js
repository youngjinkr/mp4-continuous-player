// window.setTimeout('window.location.reload()',1000); //10초마다 새로고침


var ran=Math.round(Math.random() * 100000000000000);

var currentVideo = 1;
var preVideo ;
var postVideo ;

var video = [];
var loadedVideo=[];


var playingLength = 2000; //hidden 에서 얼마나 오래 재생할 지 ?


var progressRate = 0;
var myVar;

var mins = ["0", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
			 "15", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
			 "30", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
			 "45", "", "", "", "", "", "", "", "", "", "", "", "", "", ""];
var hours = ["0am", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12pm", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"];
var currentVideotime;
var videoLength = [10,20,30,40,50,60];

var videoSec;
var hour, min;


function videoErrorHandle(e){
    switch (e.target.error.code) {
        case e.target.error.MEDIA_ERR_ABORTED:
            console.log('You aborted the video playback.');
        break;
        case e.target.error.MEDIA_ERR_NETWORK:
            console.log('A network error caused the video download to fail part-way.');
        break;
        case e.target.error.MEDIA_ERR_DECODE:
            console.log('The video playback was aborted due to a corruption problem or because the video used features your browser did not support.');
        break;
        case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
            console.log('The video could not be loaded, either because the server or network failed or because the format is not supported.');
        break;
        default:
            console.log('An unknown error occurred.');
        break;
    }
}


var ip = "192.168.11.89";

function btn_home() {
    location.replace("http://"+ ip + ":8080/www/index.html");
}

function btn_xmpp() {
    location.replace("https://"+ ip + ":80/xmpp");
}

function btn_mqtt() {
    location.replace("https://"+ ip + ":800/mqtt");
}

function btn_smartcam() {
    location.replace("http://"+ ip + ":8000/smartcam");
}

function btn_eventplay() {
    location.replace("http://"+ ip + ":8080/www/eventplay1.html");
}

function btn_contplay() {
    location.replace("http://"+ ip + ":8080/www/contplay1.html");
}
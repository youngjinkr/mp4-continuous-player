function parseJwt(jwt) {
	var payload = jwt.split('.')[1];
    var base64 = payload.replace('-', '+').replace('_', '/');
    // return payload;
    return JSON.parse(window.atob(base64)).id;
}


$(document).ready(function() {
	var myAccessToken;

  var domain = "https://api.hanwhasecurity-vsaas.com";

  $("#btn-get-accesstoken").click(function(){
    var apiUrl = domain +"/oauth2/token";

    var myAuth = "Basic " + $("#txt-auth").val();
    var myGrantType = $("#txt-grant").val();
    myWid = $("#txt-wid").val();
    myPwd = $("#txt-pwd").val();
    var myRedirect = $("#txt-redirect").val();

    var reqAccessToken = apiUrl + "?grant_type="+myGrantType + "&username=" + myWid + "&password=" + myPwd + "&redirect_uri=" + myRedirect;

    $.ajax({type:'POST', url: reqAccessToken, dataType: "json",
      headers: {
        "Content-Type":"application/json",
        "Authorization": myAuth
      },
      success: function(jsonData, textStatus, jqXHR) {
        myAccessToken = jsonData.access_token;

        console.log(myAccessToken);

       $("#txt-accesstoken1").val(myAccessToken);
      },

      error: function(jsonData, textStatus, err) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
  });

  $("#btn-reg-user").click(function(){
    var apiUrl = domain + "/internal/users";
    var userWid = $("#txt-userwid").val();
    var userPwd = $("#txt-userpwd").val();
    var userPid = $("#txt-userpid").val();
    var userName = $("#txt-username").val();

    var userEmail = $("#txt-useremail").val() + "@" + $("#txt-domain").val();
    var userStatus = $("#select-userStatus option:selected").val();
    var serviceStatus = $("#select-serviceStatus option:selected").val();    

    $.ajax({type:'POST', url: apiUrl, dataType: "json",
      headers:{
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
      },
      data: JSON.stringify(
        {
          "data": {
              "wid": userWid,
              "password": userPwd,
              "productId": userPid,
              "name": userName,
              "email": userEmail,
              "userStatus": userStatus,
              "serviceStatus": serviceStatus
          }
        }),

      success: function(jsonData, textStatus, jqXHR) {
        alert(textStatus + " to register user !");
      },

      error: function(jsonData, textStatus, err) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
  });

  $("#btn-reg-cam").click(function() {
    var apiUrl = domain + "/internal/cameras";
    var camWid = $("#txt-camwid").val();
    var userWid = $("#txt-userwid2").val();
    var camPwd = $("#txt-campwd").val();
    var camPid = $("#txt-campid").val();
    var camRegion = $("#select-region option:selected").val();
    var camName = $("#txt-camname").val();
    var serialNumber = $("#txt-serialnum").val();
    var modelName = $("#txt-modelname").val();

    //cam reg. & user attach
    if(userWid) {
      $.ajax({type: 'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization": "Bearer " + myAccessToken,
          "Content-Type": "application/json"
        },
        data: JSON.stringify(
          {
              "data": {
                  "wid": camWid,
                  "userWid": userWid,
                  "password": camPwd,
                  "productId": camPid,
                  "name": camName,
                  "productRegion": camRegion,
                  "serialNumber": serialNumber,
                  "modelName": modelName
              }
        }),
        success: function(jsonData, textStatus, jqXHR) {
          alert(textStatus + " to register camera !");
        },

        error: function(jsonData, textStatus, jqXHR) {
          var errObj = eval("("+jsonData.responseText+")");

          alert(errObj.message);
        }
      });
    }

    //cam reg without user attach
    else {
      $.ajax({type: 'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization": "Bearer " + myAccessToken,
          "Content-Type": "application/json"
        },
        data: JSON.stringify(
          {
              "data": {
                  "wid": camWid,
                  "password": camPwd,
                  "productId": camPid,
                  "name": camName,
                  "productRegion": camRegion,
                  "serialNumber": serialNumber,
                  "modelName": modelName
              }
        }),
        success: function(jsonData, textStatus, jqXHR) {
          alert(textStatus + " to register camera !");
        },

        error: function(jsonData, textStatus, jqXHR) {
          var errObj = eval("("+jsonData.responseText+")");

          alert(errObj.message);
        }
      });
    }
  });


  $("#btn-add-user2cam").click(function() {
    var apiUrl = $("#txt-endpoint4").val();
    var userWid = $("#txt-userwid3").val();

    $.ajax({type: 'PUT', url: apiUrl, dataType: "json",
      headers: {
        "Authorization": "Bearer " + myAccessToken,
        "Content-Type": "application/json; charset=UTF-8"
      },
      data: JSON.stringify({
       "data": {"userWid":userWid}
      }),
      success: function(jsonData, textStatus, jqXHR) {
          alert(textStatus + " to attach user to camera!");
      },

      error: function(jsonData, textStatus, jqXHR) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
  });
});


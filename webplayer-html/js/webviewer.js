function parseJwt(jwt) {
	var payload = jwt.split('.')[1];
    var base64 = payload.replace('-', '+').replace('_', '/');

    return JSON.parse(window.atob(base64)).id;
}


$(document).ready(function() {
	var myWid;
	var myPwd;
	var myUserId;
	var myAccessToken;
	var myCameraId;
  var myFileTimeStamp;
  var mySignedUrl;
  var domain = "https://api.hanwhasecurity-vsaas.com";


	$("#btn-get-accesstoken").click(function(){
    var apiUrl = domain +"/oauth2/token";

    var myAuth = "Basic " + $("#txt-auth").val();
    var myGrantType = $("#txt-grant").val();
		myWid = $("#txt-wid").val();
		myPwd = $("#txt-pwd").val();
    var myRedirect = $("#txt-redirect").val();

    var reqAccessToken = apiUrl + "?grant_type="+myGrantType + "&username=" + myWid + "&password=" + myPwd + "&redirect_uri=" + myRedirect;

    $.ajax({type:'POST', url: reqAccessToken, dataType: "json",
      headers: {
        "Content-Type":"application/json",
        "Authorization": myAuth
      },

      success: function(jsonData, textStatus, jqXHR) {
        myUserId = jsonData.id;
        myAccessToken = jsonData.access_token;

        console.log(myAccessToken);

       $("#txt-userid").val(myUserId);
       console.log(myUserId);

       $("#txt-accesstoken1").val(myAccessToken);
       $("#txt-accesstoken2").val(myAccessToken);
       $("#txt-accesstoken3").val(myAccessToken);
       $("#txt-accesstoken4").val(myAccessToken);            

       $("#txt-endpoint2").val("https://api.hanwhasecurity-vsaas.com/users/"+myUserId+"/cameras");
      },
      error: function(jsonData, textStatus, jqXHR) {
        var errObj = eval("("+jsonData.responseText+")");

        alert(errObj.message);
      }
    });
	});


	$("#btn-get-cameralist").click(function() {
		if($("#txt-accesstoken2").val()) {
      var apiUrl = $("#txt-endpoint2").val();
			var numberOfCameras;


      $.ajax({type:'GET', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },

        success: function(jsonData, textStatus, jqXHR) {

  				numberOfCameras = Object.keys(jsonData.data).length;

  				if(numberOfCameras===0) {
  					alert("Add cameras ! ");
  				}
  				else{
  					for (var i =0; i<=numberOfCameras-1; i++) {
  						(function(m){
  							var tag = '<a class="list-group-item" id = "list-cam'+m+'"> </a>';
  							$("#list-group-cameralist").append(tag);							
  							$("#list-cam"+m).text(jsonData.data[m].name);

  							$("#list-cam"+m).on('click',function() {
  								$("#list-cam"+m).attr("class", "list-group-item active");
                  myCameraId = jsonData.data[m].id;

  								$("#txt-selectedEndpoint").val("https://api.hanwhasecurity-vsaas.com/users/"+myUserId+"/cameras/"+myCameraId+"/evideos?limit=5");
  							});
  						})(i);
  					}
  				}
  			}
      });
		}
		else {
			alert("Get Access Token !");
		}
	});


	$("#btn-get-metadata").click(function() {

    var tag = '<div class="form-group row"> </div>';
    $("#textarea-group-metadata").html(tag);  

    var apiUrl = $("#txt-selectedEndpoint").val();

		if($("#txt-selectedEndpoint").val()){
      $.ajax({type:'GET', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },

        success: function(jsonData, textStatus, jqXHR) {
          var numberOfMetadata = Object.keys(jsonData.data).length;

  				var meta=[];

  				for(var i = 0; i<=numberOfMetadata-1;i++) {
  					(function(m) {
  						var tag = '<div class="form-group row"> <textarea class="form-control" rows="10" id="textarea-meta'+m+'" disabled=""></textarea> <button type = "button" class="btn btn-outline-primary btn-sm" data-toggle="button" aria-pressed="false" autocomplete="off" id = "btn-select-meta'+m+'">Select</button></div>';
  						$("#textarea-group-metadata").append(tag);	
  						meta[m] = JSON.stringify(jsonData.data[m], undefined, 10); 

  						$("#textarea-meta"+m).text(meta[m]);
  						
  						$("#btn-select-meta"+m).on('click',function() {
                  myFileTimeStamp = jsonData.data[m].fileTimestamp;
  						});						
  					})(i);
  				}
        }
			});
	
		}
		else{
			alert("Get Camera list !");
		}
	});


  $("#btn-get-storageToken").click(function() {
    if(myFileTimeStamp) {
      var apiUrl = domain +"/storage1/token";
      $.ajax({type:'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+myAccessToken,
          "Content-Type":"application/json"
        },
        data: JSON.stringify({
          "cameraId": myCameraId,
          "fileTimestamp": myFileTimeStamp,
        }),

        success: function(jsonData, textStatus, jqXHR) {
          mySignedUrl =jsonData.data.storageLocation;
          $("#txt-signedUrl").val(mySignedUrl);
          console.log("signed url : \n"+mySignedUrl);
        }
      });      
    }
    else{
      alert("Select the metadata !");
    }
  });


  $("#btn-get-file").click(function() {
     $('<a/>',{
     "href":mySignedUrl,
    "download":"htw-vsaas-evideo.mp4",
    id:"videoDownloadLink"
    }).appendTo(document.body);
  $('#videoDownloadLink').get(0).click()
  });


  $("#btn-play-file").click(function() {
    var myVideo = document.getElementById("video-event");
    myVideo.src = mySignedUrl;
    myVideo.play();

  })
});


$(document).ready(function() {

var urlArray =[];


function parseJwt(jwt) {
  var payload = jwt.split('.')[1];
    var base64 = payload.replace('-', '+').replace('_', '/');
    // return payload;
    return JSON.parse(window.atob(base64)).id;
}
  var domain = "https://api.hanwhasecurity-vsaas.com";

  $("#btn-get-accesstoken").click(function(){
    getAccessToken()
      .then(getUserId)
      .then(getCameraId)
      .then(getMetadata)
      .then(function(retObj){
        var defer = new Promise(function(resolve, reject) {
          var requestVideo = function(){
            var newObj = {
              fileTimestamp: retObj.fileTimestamp.pop(),
              cameraId: retObj.cameraId,
              token: retObj.token
            };
            getStorageToken(newObj)
              .then(makeVideoTag)
              .then(function(){
                  if(retObj.fileTimestamp.length > 0){
                    requestVideo();
                  }else{
                    console.log("Finish");
                    resolve("Finish");
                  }
                });
          };
          requestVideo();
          });
        return defer;
      })
      .then(addEvent)      
      .then(playVideo)


  });



  function getAccessToken() {
    var apiUrl = domain +"/oauth2/token";

    var myAuth = "Basic " + $("#txt-auth").val();
    var myGrantType = $("#txt-grant").val();
    myWid = $("#txt-wid").val();
    myPwd = $("#txt-pwd").val();
    var myRedirect = $("#txt-redirect").val();

    var reqAccessToken = apiUrl + "?grant_type="+myGrantType + "&username=" + myWid + "&password=" + myPwd + "&redirect_uri=" + myRedirect;

    var defer = new Promise(function(resolve, reject){
      $.ajax({type:'POST', url: reqAccessToken, dataType: "json",
        headers: {
          "Content-Type":"application/json",
          "Authorization": myAuth          
        },

        success: function(jsonData, textStatus, jqXHR) {
          var myAccessToken = jsonData.access_token;

          resolve(myAccessToken);

          console.log(myAccessToken);
        },
      });
    });
    return defer;
  }

  function getUserId(token) {
    var userId = parseJwt(token);
    var retObj = {
      token: token,
      userId: userId
    };
    
    console.log(retObj);
    return Promise.resolve(retObj);
  }


  function getCameraId(object) {
    var apiUrl = domain + "/users/" + object.userId + "/cameras";
    var numberOfCameras;
    var myCameraId;
    var retObj = {
      token: object.token,
      userId: object.userId,
    };


    var defer = new Promise(function(resolve, reject){
      $.ajax({type:'GET', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+ object.token,
          "Content-Type":"application/json"
        },

        success: function(jsonData, textStatus, jqXHR) {
          // input the camera list with the result...
          
          numberOfCameras = Object.keys(jsonData.data).length;

          if(numberOfCameras===0) {
            alert("Add cameras ! ");
          }
          else{
                  retObj.cameraId = jsonData.data[0].id;

                  console.log(retObj);
                  resolve(retObj);
          }
        }
      });      
    });
    return defer;
  }

  function getMetadata(object) {
    var apiUrl = domain + "/users/" + object.userId + "/cameras/" + object.cameraId + "/evideos?orderby=desc";
    var retObj = {
      token: object.token,
      cameraId: object.cameraId
    };

    var defer = new Promise(function(resolve, reject) {
      $.ajax({type:'GET', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+ object.token,
          "Content-Type":"application/json"
        },

        success: function(jsonData, textStatus, jqXHR) {
          var numberOfMetadata = Object.keys(jsonData.data).length;
          var meta=[];
          var fileTimestamp=[];

          for(var i =0; i<=numberOfMetadata-1;i++){
            (function(m) {
              meta[m] = JSON.stringify(jsonData.data[m], undefined, 10); 
              fileTimestamp[m] = jsonData.data[m].fileTimestamp;

            })(i);
          }
          retObj.meta = meta;
          retObj.fileTimestamp = fileTimestamp;
          resolve(retObj);
        },
        error: function() {
        }
      });
    });
    return defer;
  }


  function getStorageToken(object) {
    var apiUrl = domain + "/storage1/token";
    var fileTimestamp = object.fileTimestamp;
    var urlArray=[];

    console.log(fileTimestamp);

    var defer = new Promise(function(resolve, reject) {

      $.ajax({type:'POST', url: apiUrl, dataType: "json",
        headers: {
          "Authorization":"Bearer "+ object.token,
          "Content-Type":"application/json"
        },
        data: JSON.stringify({
          "cameraId": object.cameraId,
          "fileTimestamp": fileTimestamp,
        }),

        success: function(jsonData, textStatus, jqXHR) {
          mySignedUrl = jsonData.data.storageLocation;

          resolve(mySignedUrl);
        }
      });  

    });
    return defer;
  }

  function makeVideoTag(url) {
    var defer = new Promise(function(resolve, reject) {
      var tag = '<video id = "myVideo'+urlArray.length+ '"  style= "display:none;" src = "'+url+'"></video>';
      $("#videoDiv").append(tag);

      var myVideo = document.getElementById("myVideo" + urlArray.length);

      urlArray.push(url);
      console.log(urlArray);
      

      var sourceLength = urlArray.length;
      resolve("finish");
    });
    return defer;
  }


  function addEvent() {

    for(var i = 0; i< urlArray.length; i++){
      (function(m){
        var nextVideo;
        var doubleNextVideo;
        var nowVideo = document.getElementById("myVideo" + m);

        if(m==urlArray.length-1) {
          if(urlArray.length == 1) {
            nowVideo.style.display = "";
          }

          nowVideo.onended= function(){
            console.log((m+1) + " video is finished playing !! ");
          }
        }

        else if(m==0 && urlArray.length !=1) {
          nowVideo.style.display = '';
          nowVideo.onended= function(){
            nowVideo.style.display = "none";
            nextVideo = document.getElementById("myVideo" + (m+1));
            nextVideo.style.display = '';
            nextVideo.play();

            if(urlArray.length>3){
              doubleNextVideo = document.getElementById("myVideo" + (m+2));
            
              setTimeout(function() {
                doubleNextVideo.muted = true;
                doubleNextVideo.play();      

                setTimeout(function(){
                  doubleNextVideo.pause();
                  doubleNextVideo.muted = false;
                  doubleNextVideo.currentTime = "0";
                },1000);
              },6000);            
            }
          }
        }

        else if(m==urlArray.length-2) {
          nowVideo.onended= function(){
            nowVideo.style.display = "none";
            nextVideo = document.getElementById("myVideo" + (m+1));
            nextVideo.style.display = '';
            nextVideo.play();          
          }
        }

        else{
          nowVideo.onended= function(){
            nowVideo.style.display = "none";
            nextVideo = document.getElementById("myVideo" + (m+1));
            nextVideo.style.display = '';
            nextVideo.play();

            doubleNextVideo = document.getElementById("myVideo" + (m+2));
          
            setTimeout(function() {
              doubleNextVideo.muted = true;
              doubleNextVideo.play();      

              setTimeout(function(){
                doubleNextVideo.pause();
                doubleNextVideo.muted = false;
                doubleNextVideo.currentTime = "0";
              },1000);
            },7000);
          }
        }
      })(i);
    }
  }

  function playVideo() {
    var defer = new Promise(function(resolve, reject) {
      var firstVideo = document.getElementById("myVideo0");
      firstVideo.play();  

      if(urlArray.length >=2) {
        var secondVideo = document.getElementById("myVideo1");

        setTimeout(function() {
          secondVideo.muted = true;
          secondVideo.play();

          setTimeout(function(){
            secondVideo.pause();
            secondVideo.muted = false;
            secondVideo.currentTime = "0";
          },1000);
        },9000);              
      }
    });
    return defer;
  }
});


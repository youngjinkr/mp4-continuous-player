var tempAccessKeyId, tempSecretAccessKey, tempSessionToken;

var bucketName = 'cloud-video-storage';
var cameraName = 'mycam1';
// var fakeName = 'mycam1';

var regionName = 'ap-northeast-2';




function makeDate() {

    var d = new Date();
    var day = (d.getDate()).toString();
    var month = ((d.getMonth())+1).toString();
    var year = (d.getFullYear()).toString();

    if(day<10){
        day = '0'+ day;
    }

    if(month<10) {
        month = '0' + month;
    }


    date = (day + month + year);
    // console.log(date);

    return date;
}

// console.log(makeDate());  //test 용
// console.log("cameraName : " + cameraName);

makePath('1.mp4');   //test 용

//s3에 저장할 때, 어떤 경로에 저장할 건지 만들어주는 함수
function makePath(fn) {

    var path;
    var cameraModel = 'SNH-V6414BN';
    var randomNumber = Math.round(Math.random()*10000000);
    var fileName= fn;

    
    path = makeDate() + '/' + 'event-video/v1/' + cameraModel + '/388591930080:'+ cameraName + '/' + fileName +randomNumber;

    // console.log(path)
    return path;
}

//S3 접근에 필요한 인증 정보들을 집어 넣는 함수
function input() {
    tempAccessKeyId = document.getElementById("txt_accesskeyid").value;
    tempSecretAccessKey = document.getElementById("txt_secretaccesskey").value;
    tempSessionToken = document.getElementById("area_sessiontoken").value;

    if(tempAccessKeyId && tempSecretAccessKey && tempSessionToken)
        alert("[SUCCESS] : input")
    else
        alert("[FAILURE]");


}

//인증 정보를 가지고 S3에 접근할 수 있도록 정보 업데이트
function signToS3() {
    AWS.config.update({
        region: regionName,
        accessKeyId: tempAccessKeyId,
        secretAccessKey: tempSecretAccessKey,
        sessionToken: tempSessionToken,
        
    });
}


//s3에 업로드 하는 함수
function uploadFile() {

    signToS3();

    var files = document.getElementById("uploader").files;
    var file = files[0];
    var fileName = file.name;
    var fileType = file.type;



    // var path = makeDate() + '388591930080:'+cameraName;
    var path = '388591930080:'+cameraName;
    var filePath = path + '/';
    var pathKey = filePath + fileName;
    


    var s3 = new AWS.S3();

    s3.putObject({
        Body: file,
        Bucket: bucketName,
        Key: pathKey
    }, function(err, data) {
        if(err) {
            console.log(err, err.stack);

        }
        else {
            console.log("[Success] File Upload");
        }
    });


var temp_auth = AWS.config.credentials;
console.log(temp_auth);






}

//S3에서 정해진 파일을 다운 받는 함수
function downloadFile() {

    signToS3();

    var s3 = new AWS.S3();

    var path = '388591930080:'+cameraName;
    var filePath = path + '/';
    var fileName = '001.mp4';
    var pathKey = filePath + fileName;

    var param = {
        Bucket: bucketName,
        Key: pathKey
    };

    var url = s3.getSignedUrl('getObject', param);
    console.log("url : " + url);


    document.getElementById('myiframe').src = url;



    




}

//S3에서 파일을 받아 비디오 태그에 넣고 재생하는 함수
function playVideo() {

    signToS3();

    var video = document.getElementById('myvideo');
    var s3 = new AWS.S3();

    var path = '388591930080:'+cameraName;
    var filePath = path + '/';
    var fileName = '001.mp4';
    var pathKey = filePath + fileName;

    var param = {
        Bucket: bucketName,
        Key: pathKey
    };

    var url = s3.getSignedUrl('getObject', param);

    video.src = url;

    video.oncanplaythrough = function() {
        video.play();
    }
}





//api 서버 용으로 만들어놓은 함수, Camera와 WEB 용 인증으로는 작동하지 않아야 함
function deleteFile() {

}